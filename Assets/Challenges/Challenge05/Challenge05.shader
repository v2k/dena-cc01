﻿Shader "ShaderChallenge/Challenge05" {

	Properties {
		_Tex ("Base", 2D) = "white" {}
		_Mask1 ("Mask1", 2D) = "white" {}
		_Mask2 ("Mask2", 2D) = "white" {}
		_Mask3 ("Mask3", 2D) = "white" {}
		_Mask4 ("Mask4", 2D) = "white" {}
		_Color1 ("Color1", Color) = (1,1,1,1)
		_Color2 ("Color2", Color) = (1,1,1,1)
		_Color3 ("Color3", Color) = (1,1,1,1)
		_Color4 ("Color4", Color) = (1,1,1,1)
	}

	SubShader {

		Pass {
			CGPROGRAM

			#pragma vertex vertex_shader
			#pragma fragment fragment_shader

			///////////////////////////

			struct vertex_in {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct vert_to_frag {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			sampler2D _Tex;
			sampler2D _Mask1;
			sampler2D _Mask2;
			sampler2D _Mask3;
			sampler2D _Mask4;
			float4 _Color1;
			float4 _Color2;
			float4 _Color3;
			float4 _Color4;

			///////////////////////////

			vert_to_frag vertex_shader(vertex_in v)
			{
				vert_to_frag o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.texcoord.xy;
				return o;
			}

			///////////////////////////

			half4 fragment_shader(vert_to_frag i) : COLOR {

				half4 mask1 = tex2D(_Mask1, i.uv);;
				half4 mask2 = tex2D(_Mask2, i.uv);
				half4 mask3 = tex2D(_Mask3, i.uv);
				half4 mask4 = tex2D(_Mask4, i.uv);

				half4 tex = tex2D(_Tex, i.uv) * (1-mask1) * (1-mask2) * (1-mask3) * (1-mask4)
					+ mask1 *_Color1
					+ mask2 *_Color2
					+ mask3 *_Color3
					+ mask4 *_Color4;

				return tex;
			}

			ENDCG
		}
	}
}



