﻿Shader "ShaderChallenge/Challenge02" {
    
    Properties {
		_MyColor ("Some Color", Color) = (1,1,1,1)
		_MyTex ("Texture", 2D) = "white" {}
    }
    
    SubShader {
    
        Pass {
            CGPROGRAM

			#pragma vertex vertex_shader
            #pragma fragment fragment_shader
            
            ///////////////////////////
            
			struct vertex_in {
			    float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vert_to_frag {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			
			sampler2D _MyTex;
			float4 _MyColor;

			///////////////////////////

			vert_to_frag vertex_shader( vertex_in v )
			{
				vert_to_frag o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = float4 (v.texcoord.xy, 0, 0);
				return o;
			}
			
			///////////////////////////
			
            half4 fragment_shader(vert_to_frag i) : COLOR {
				half4 tex = tex2D (_MyTex, i.uv);
				return tex * _MyColor;
            }
	
            ENDCG
        }
    }
}
