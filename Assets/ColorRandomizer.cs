﻿using UnityEngine;
using System.Collections;

public class ColorRandomizer : MonoBehaviour {
	
	Color mColor1;
	Color mColor2;
	Color mColor3;
	Color mColor4;

	Color mOrgColor1;
	Color mOrgColor2;
	Color mOrgColor3;
	Color mOrgColor4;

	// Use this for initialization
	void Start () {
		mOrgColor1 = mColor1 = renderer.material.GetColor("_Color1");
		mOrgColor2 = mColor2 = renderer.material.GetColor("_Color2");
		mOrgColor3 = mColor3 = renderer.material.GetColor("_Color3");
		mOrgColor4 = mColor4 = renderer.material.GetColor("_Color4");
	}
	
	// Update is called once per frame
	void Update () {
		renderer.material.SetColor("_Color1", mColor1);
		renderer.material.SetColor("_Color2", mColor2);
		renderer.material.SetColor("_Color3", mColor3);
		renderer.material.SetColor("_Color4", mColor4);
		float x = Mathf.Abs(Mathf.Sin(Time.time));
		mColor1 = Color.Lerp(mOrgColor1, mOrgColor2, x);
		mColor2 = Color.Lerp(mOrgColor2, mOrgColor3, x);
		mColor3 = Color.Lerp(mOrgColor3, mOrgColor4, x);
		mColor4 = Color.Lerp(mOrgColor4, mOrgColor1, x);
	}
}
