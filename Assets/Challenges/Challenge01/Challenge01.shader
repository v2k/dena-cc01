﻿Shader "ShaderChallenge/Challenge01" {
    
    Properties {
		_MyColor ("Some Color", Color) = (1,1,1,1) 
    }
    
    SubShader {
    
        Pass {
            CGPROGRAM

			#pragma vertex vertex_shader
            #pragma fragment fragment_shader
            
            ///////////////////////////
            
			struct vertex_in {
			    float4 vertex : POSITION;

			};
			
			struct vert_to_frag {
				float4 pos : SV_POSITION;
			};

			float4 _MyColor;
			
			///////////////////////////

			vert_to_frag vertex_shader( vertex_in v )
			{
				vert_to_frag o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			///////////////////////////
			
            float4 fragment_shader(vert_to_frag i) : COLOR {
           	 	return _MyColor;
            }

            ENDCG
        }
    }
}
