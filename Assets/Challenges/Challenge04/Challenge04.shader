﻿Shader "ShaderChallenge/Challenge04" {
    
    Properties {
		_MyTexA ("Texture", 2D) = "white" {}
		_MyTexB ("Texture", 2D) = "white" {}
		_MyTexBlend ("Texture", 2D) = "white" {}
    }
    
    SubShader {
    
        Pass {
            CGPROGRAM

			#pragma vertex vertex_shader
            #pragma fragment fragment_shader
            
            ///////////////////////////
            
			struct vertex_in {
			    float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vert_to_frag {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			
			sampler2D _MyTexA;
			sampler2D _MyTexB;
			sampler2D _MyTexBlend;

			///////////////////////////

			vert_to_frag vertex_shader( vertex_in v )
			{
				vert_to_frag o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.texcoord.xy;
				return o;
			}
			
			///////////////////////////
			
            half4 fragment_shader(vert_to_frag i) : COLOR {
				half4 blend = tex2D(_MyTexBlend, i.uv);
				half4 tex = tex2D(_MyTexA, i.uv) * blend + tex2D(_MyTexB, i.uv) * (1 - blend);
				return tex;
            }
	
            ENDCG
        }
    }
}


