﻿Shader "ShaderChallenge/Challenge06" {
    
    Properties {
    	// none yet!
    }
    
    SubShader {
    
        Pass {
            CGPROGRAM

			#pragma vertex vertex_shader
            #pragma fragment fragment_shader
            
            ///////////////////////////
            
			struct vertex_in {
			    float4 vertex : POSITION;
			};
			
			struct vert_to_frag {
				float4 pos : SV_POSITION;
			};
			
			///////////////////////////

			vert_to_frag vertex_shader( vertex_in v )
			{
				vert_to_frag o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			///////////////////////////
			
            float4 fragment_shader(vert_to_frag i) : COLOR {
           	 	return float4(1.0f, 1.0f, 1.0f, 1.0f);
            }

            ENDCG
        }
    }
}